import os
import subprocess
import yaml
import shutil
from prov_agent.module.base import ModuleBase


class Puppet(ModuleBase):
    def __init__(self, name, url, repo='git', ref='master', config=None):
        """
        :param name: name of the module
        :param module_cache: Module cache directory from where the module is installed
        :param meta: metadata about the module
        :param module: module data - extracted from metadata
        :param site_manifest: site manifest file path relative path from manifests_dir (/etc/puppet/manifests/<module name>/)
        """
        super(Puppet, self).__init__(name, url, repo, ref, config)
        self.name = name.replace("-", "_")
        self.manifests_dir = "/etc/puppet/manifests"
        self.module_dir = "/etc/puppet/modules/%s" % self.name
        self.config_base = "/etc/puppet"
        self.site_manifest = self.config.get("manifest", "/etc/puppet/manifests/site.pp")
        self.hieradata = self.config.get("hieradata", "/etc/puppet/hieradata")
        # TODO: may be there are more advanced usecases to get multiple hierarchy from module
        self.hiera_cache = os.path.join(self.module_dir, 'hiera', 'common.yaml')

    def install(self, properties=None):
        rv = None
        module_installed = self._install_module(self.url, repo=self.repo, ref=self.ref)

        if properties:
            user_yaml = os.path.join(self.hieradata,'user.yaml')
            try:
                existing_config = yaml.load(file(user_yaml, 'r'))
            except yaml.YAMLError, e:
                print "Error in existing user hiera: ", e
                return False
            if not existing_config:
                existing_config = {}
            existing_config.update(properties)
            stream = file(user_yaml, 'w')
            yaml.safe_dump(existing_config, stream, default_flow_style=False)

        with open(self.site_manifest, 'r') as f:
            lines = f.readlines()
        manifest_entries = set(lines)
        manifest_entries.add("include ::%s\n" % self.name)
        with open(self.site_manifest, 'w') as f:
            f.write("".join(manifest_entries))
        return rv

    def _install_module(self, url, name=None, repo=None, ref=None,
                        module_path='/etc/puppet/modules'):
        if not name:
            name = os.path.basename(url)

        download_path = os.path.join(module_path, name.replace("-", "_"))
        if os.path.exists(download_path):
            print "Module already installed"
            return True

        module_cache = self._get_module(name, url, repo, ref)
        if module_cache:
            try:
                puppet_dir = os.path.join(module_cache, 'puppet')
                hiera = os.path.join(puppet_dir, 'hiera', 'common.yaml')
                shutil.copytree(puppet_dir, download_path)
                puppetfile = os.path.join(puppet_dir, 'Puppetfile')
                if os.path.isfile(puppetfile):
                    proc = subprocess.Popen(
                        ["librarian-puppet", "install", "--puppetfile=" + puppetfile],
                        cwd=self.config_base)
                    rv = proc.wait()

                if os.path.isfile(os.path.join(hiera)):
                    if not self._merge_hiera(hiera):
                        print "Copying common hiera data from module has been failed"

            except Exception as e:
                print "Module download failed\n %s" % e
                return False

            metadata = os.path.join(module_cache, 'metadata.yaml')
            if os.path.isfile(metadata):
                with open(metadata, 'r') as stream:
                    meta = yaml.load(stream)
                for mod_name, mod_data in meta.get('require', {}).iteritems():
                    repo = None
                    url = None
                    ref = None
                    for prop, value in mod_data.iteritems():
                        if prop == 'git':
                            repo = prop
                            url = value
                        elif prop == 'ref':
                            ref = value
                    self._install_module(url, repo=repo, ref=ref)
            return True
        else:
            print "Error: failed downloading the module"
            return False


    def _merge_hiera(self, src, dst=None):
        if not dst:
            dst = os.path.join(self.hieradata, 'common.yaml')
        try:
            src_dict = yaml.load(file(src, 'r'))
        except yaml.YAMLError, e:
            print "Error in source yaml: ", e
            return False
        try:
            dst_dict = yaml.load(file(dst, 'r'))
        except yaml.YAMLError, e:
            print "Error in dst yaml: ", e
            return False
        if not dst_dict:
            dst_dict = {}
        if not src_dict:
            src_dict = {}
        dst_dict.update(src_dict)
        dst_stream = file(dst, 'w')
        yaml.safe_dump(dst_dict, dst_stream, default_flow_style=False)
        return True

    def deploy(self):
        if os.path.isfile(self.site_manifest):
            proc = subprocess.Popen(
                ["puppet", "apply", "--detailed-exitcodes",
                 self.site_manifest])
            return proc.wait()
        else:
            print "Manifest file doesn't exists"
            return False
