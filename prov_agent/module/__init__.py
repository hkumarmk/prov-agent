import os
import yaml
import re
import shutil

from prov_agent.module.puppet import Puppet


class Module(object):
    modules = {}

    def __init__(self, name, url='http://prov-controller/modules/',
                 repo=None, ref=None, config=None):
        Module.modules.update({name: self})
        self.name = name
        self.url = url
        self.repo = repo
        self.ref = ref or 'master'
        self.config = config
        # TODO: Eventually we would be supporting multiple providers
        # There might be the cases where different modules will be with different providers.
        self.provider = Puppet(self.name, self.url, self.repo, self.ref, self.config)

    def get_provider(self):
        return self.provider