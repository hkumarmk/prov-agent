import re
import os
import subprocess

from tempfile import mkdtemp

class ModuleBase(object):

    def __init__(self, name, url=None, repo=None, ref=None, config=None):
        self.url = url
        self.repo = repo
        self.ref = ref or 'master'
        self.name = name
        self.config = config or {}

    @classmethod
    def serializable(cls, data):
        if isinstance(data, cls):
            return data.details
        elif isinstance(data, dict):
            result = {}
            for k, v in data.iteritems():
                if isinstance(v, cls):
                    result.update({k: v.details})
            return result
        elif isinstance(data, (str, int, basestring, bool)):
            return {'response': data}

    def _git_clone(self, repo, path, ref='master'):
        proc = subprocess.Popen(
                ["git", "clone", "-b", ref, repo, path])
        rv = proc.wait()
        return not bool(rv)

    def _get_module(self, name, url, repo=None, ref=None):
        """
        :param download_path: Local filesystem path to download the module to
        :return: local filesystem path to the module
        """
        #TODO: prov-controller may act as a cache, may be we could use things like bittorrent

        abs_path = None
        if re.match(r'^file://', url):
            abs_path = re.sub('file://', '', url)
            if not re.match(r'^/', abs_path):
                abs_path = '/' + abs_path
        elif re.match(r'^/', url):
            abs_path = os.path.abspath(url)
        elif repo == 'git':
            abs_path = os.path.join(mkdtemp(), name)
            os.makedirs(abs_path)
            if not self._git_clone(url, abs_path):
                print "Failed downloading the module"
                return None
        elif re.match(r'^(http|https)://', url):
            # TODO: Need to have code to download module from web url
            #abs_path = os.path.basename(url)
            pass

        if os.path.exists(abs_path):
            return abs_path
        else:
            print "The path %s does not exist" % abs_path
            return None
