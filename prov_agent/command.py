import sys
import zmq
import argparse
import yaml
import json
import os.path


def load_json(myjson):
    json_object = json.loads(myjson)
    return json_object

def main(args=sys.argv[1:]):

    ap_name = argparse.ArgumentParser(add_help=False)
    ap_name.add_argument('name', type=str,
                         help='Name of the object')

    ap = argparse.ArgumentParser(description='Prov client')
    sp = ap.add_subparsers(dest='resource', help='Resource to manage')
    p_module = sp.add_parser('module', help='manage modules')
    sp_module = p_module.add_subparsers(dest='action')
    p_module_install = sp_module.add_parser('install', parents=[ap_name],
                                            help="Install module")
    p_module_install.add_argument('-u', '--url', type=str,
                                  help="module url")
    p_module_install.add_argument('-g', '--git-repo', type=str,
                                  help="Git repo url of the module")
    p_module_deploy = sp_module.add_parser('deploy', parents=[ap_name],
                                            help="Deploy module")
    p_module_install_group = p_module_install.add_mutually_exclusive_group()
    p_module_install_group.add_argument('-P', '--property', type=str,
                                 help="Configurations as json inputs")
    p_module_install_group.add_argument('-p', '--property-file', type=str,
                                 help="Configurations as json inputs")
    args = ap.parse_args()

    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("ipc:///tmp/filename")

    if args.resource == 'module':
        msg = {'resource': args.resource, 'oper': args.action, 'name': args.name}
        if args.action == 'install':
            if args.url:
                msg.update({'url': args.url})
            elif args.git_repo:
                msg.update({'url': args.git_repo})
                msg.update({'repo': 'git'})
            property_dict = None
            if args.property:
                property_dict = load_json(args.property)
            elif args.property_file:
                property_file = os.path.abspath(args.property_file)
                if os.path.isfile(property_file):
                    stream = file(property_file, 'r')
                    try:
                        property_dict = yaml.load(stream)
                    except Exception as e:
                        print(e)
                else:
                    raise "property_file (%s) doesn't exist" % property_file

            if property_dict:
                msg.update({'properties': property_dict})

    print "Sending", msg
    socket.send_json(msg)
    msg_in = socket.recv_json()
    print msg_in

if __name__ == '__main__':
    sys.exit(not main(sys.argv[1:]))
