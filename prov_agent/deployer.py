import yaml
import subprocess
import os

class Deployer(object):
    def __init__(self):
        self.deployer = None

    @staticmethod
    def get_provider(provider='puppet'):
        if provider == 'puppet':
            return Puppet()
        else:
            return None


class Puppet(object):

    def __init__(self):
        self.hiera_config = subprocess.check_output(['puppet','config','print','hiera_config']).strip()
        self.hieradata = '/etc/puppet/hieradata'
        self.manifest = subprocess.check_output(['puppet','config','print','manifest']).strip()
        self.module_config = {'hiera_config': self.hiera_config,
                              'hieradata': self.hieradata,
                              'manifest': self.manifest}
        self.install()
        self.config()

    def install(self):
        #TODO This should support multiple operating system, right now only debian flavors are handled.
        # As of now assumeing puppet is already installed.
        pass

    def config(self):
        self.make_hiera_config()
        if not os.path.exists(self.hieradata):
            os.makedirs(self.hieradata)
        open(self.manifest, "a")
        open(os.path.join(self.hieradata, "common.yaml"), "a")
        open(os.path.join(self.hieradata, "user.yaml"), "a")
        self._gem('librarian-puppet-simple')

    @staticmethod
    def _gem(package, ensure="present"):
        cmd = "gem list | grep -q '^%s '" % package
        proc = subprocess.Popen(cmd, shell=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        rv = proc.wait()
        if rv != 0 and ensure in ('present', 'installed'):
            cmd = ["gem", "install", "--no-ri", "--no-rdoc", package]
            subprocess.check_call(cmd)
        elif rv == 0 and ensure == ('absent', 'uninstalled'):
            cmd = "gem -aIx %s" % package
            subprocess.check_call(cmd)

    def make_hiera_config(self):
        ## There must be a deployer class and this code must be part of that as this is a common thing (one time)
        hiera_config = {
            ':backends': ['yaml'],
            ':yaml': {':datadir': self.hieradata},
            ':hierarchy': ['user','common']
        }
        stream = file(self.hiera_config, 'w')
        yaml.safe_dump(hiera_config, stream, default_flow_style=False)
